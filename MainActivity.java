package com.example.recycleviewptoject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        List<Books> booksList = new ArrayList<>();
        booksList.add(new Books ("Quran Kareem",R.drawable.quran,
                "https://quran.com/"));
        booksList.add(new Books ("Ibn-Kathir Tasir Quran",R.drawable.ibnkathir,
                "https://shamela.ws/index.php/author/3"));
        booksList.add(new Books ("Physics",R.drawable.physics,
                "https://en.wikipedia.org/wiki/Physics"));
        booksList.add(new Books("Calculus",R.drawable.calculus,
                "https://en.wikipedia.org/wiki/Calculus"));
        booksList.add(new Books("Algorithms",R.drawable.algorithms,
                "https://classroom.udacity.com/courses/ud061/lessons/3521808661/concepts/24783585380923"));
        //Setting up new Adapter and invoking the pre-defined list into it.
        BookAdapter adapter = new BookAdapter(booksList);
        //Linking with the Recycler XML file.
        RecyclerView recycler = findViewById(R.id.recyclerView);
        //Setting the context with method <setLayoutManager>
        recycler.setLayoutManager(new LinearLayoutManager(this));
        //Setting the adapter and linking it to the recycler
        recycler.setAdapter(adapter);
        //invoking the setOnItemClickListener with adapter
        adapter.setOnItemClickListener(new BookAdapter.OnItemClickListener(){
            @Override
            public void OnItemClick(int position){
                Books book = booksList.get(position);
                Intent n = new Intent();
                n.setAction(Intent.ACTION_VIEW);
                n.setData(Uri.parse(book.getUrlID()));
                startActivity(n);
            }
        });
    }
}
package com.example.recycleviewptoject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.BookHolder> {

    //implementing onItemClickListener <because Recycler View doesn't contain it>.
    private OnItemClickListener onItemClickListener;
    //Setting up setOnItemClickListener <according the pre-identified interface below>
    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }
    private List<Books> bookList;
    //implementing a new Constructor then invoking the list to be the source of data.
    public BookAdapter(List<Books> bookList){
        this.bookList=bookList;
    }
    //Binding data retrieved from XML file into this method
    @NonNull
    @Override
    public BookHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //Convert the view of the XML file to be such convenient to class files , moreover we refer
        //to with class type variable <View Class> {{View class is a comprehensive class for
        // each such view.}}
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_book_adapter,
                parent,false);
        //while BookHolder [inner class] contains variable of XML attributes; we implement new object
        //from it to pass the View variable into it; which contains the converted XML view.
        BookHolder hold = new BookHolder(v);
        return hold;
    }

    @Override
    public void onBindViewHolder(@NonNull BookHolder holder, int position) {
        //set data which will be bind into the list.
        Books book = bookList.get(position);
        String name = book.getBookName();
        int id = book.getBookId();
        //attaching each variable from getters methods into the holder.
        holder.textName.setText(name);
        holder.imgName.setImageResource(id);
    }

    @Override
    public int getItemCount() {
        return bookList.size();
    }
    
    //inner class
    /*Extended class that used to retrieve data from XML file and setting up the {setOnClickListener}
      which to be used in recyclerView which is never implemented on.
    * */
    class BookHolder extends RecyclerView.ViewHolder{
        //referring to references on XML file
        private TextView textName;
        private ImageView imgName;
        public BookHolder(@NonNull View itemView) {
            super(itemView);
            //linking the XML views to class variables
            textName = itemView.findViewById(R.id.textVyw);
            imgName = itemView.findViewById(R.id.imgVyw);

            //setting method <setOnItemClickListener>
            itemView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick (View v){
                    if(onItemClickListener != null && getAdapterPosition() != RecyclerView.NO_POSITION){
                        onItemClickListener.OnItemClick(getAdapterPosition());
                    }
                }
                });
            }
        }
        //implementing OnItemClickListener interface
        interface OnItemClickListener {
            void OnItemClick(int position);
    }

}
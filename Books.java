package com.example.recycleviewptoject;

public class Books {
    private String bookName;
    private int bookId;
    private String urlID;
    public Books (String bookName,int bookId,String urlID){
        this.bookName = bookName;
        this.bookId = bookId;
        this.urlID = urlID;
    }
    //setting getters
    public String getBookName(){
        return bookName;
    }
    public int getBookId(){
        return bookId;
    }
    public String getUrlID(){return urlID;}
}

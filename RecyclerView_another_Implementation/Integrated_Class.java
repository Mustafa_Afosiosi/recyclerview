

public class ClassName {
	private Integer itemLogo;
	private String itemName;


	//Constructor {Considered as Setters of the variables}...
	public ClassName(Integer itemLogo, String itemName){

		this.itemLogo = itemLogo;
		this.itemName = itemName;
	}

	public Integer getItemLogo(){
		return itemLogo;
	}

	public String getItemName(){
		return itemName;
	}
}

class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder>{
	
	
	ArrayList<ClassName> clss_arr;
	Context context;

	public RecyclerAdapter(Context context, ArrayList<ClassName> itemArray){
		this.context = context;
		clss_arr = itemArray;
	}

	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
		View view = LayoutInflater.from(parent.getContext())
					.inflate(R.layout.row_item, parent, false);
		return null;
	}


	@Override
	public void onBindViewHolder(@NonNull ViewHolder holder, int position){

		holder.imageView.setImageResource(clss_arr.get(position).getItemLogo());

		holder.textView.setText(clss_arr.get(position).getItemName());
	}


	@Override
	public int getItemCount(){
		return clss_arr.size();
	}



	public class ViewHolder extends RecyclerView.ViewHolder{
		//Initialise Views
		ImageView imageView;

		TextView textView;

		public ViewHolder(@NonNull View itemView){
			super(itemView);

			//Assign Variables
			imageView = itemView.findViewById(R.id.image_view);
			textView = itemView.findViewById(R.id.textView);
			

		}
	}
}
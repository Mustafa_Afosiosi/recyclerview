public class MainActivity extends AppCompatActivity {
	
	RecyclerView recyclerView;
	ArrayList<ClassName> itemsArray;
	RecyclerAdapter recycler_adapter;

	@Override
	protected void onCreate (Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);


		recyclerView = findViewById(R.id.recycler_view);

		Integer[] item_arr = {};

		String[] item_nm_arr = {};

		//Initialise ArrayList...

		itemsArray = new ArrayList<>();

		for (int i=0; i<itemLogo.length; i++){
			ClassName className = new ClassName(itemLogo[i], itemName[i]);
			itemsArray.add(className);

		}
		// Design Horizontal Layout
		LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false);

		recyclerView.setLayoutManager(layoutManager);
		recyclerView.setItemAnimator(new DefaultItemAnimator());


		//Initialise the Adapter
		recyclerAdapter = new RecyclerAdapter(MainActivity.this, itemsArray);

		recyclerView.setAdapter(recyclerAdapter);


	}
}
